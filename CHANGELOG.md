# CHANGELOG | 变更日志

## [1.0.2](https://gitee.com/WuCheng-cn/any-thing/compare/1.0.1...1.0.2) (2025-03-07)

### ⚡️ Performance Improvements | 性能优化

* **custom:** 去除弃用依赖standrad-version ([d2b5278](https://gitee.com/WuCheng-cn/any-thing/commit/d2b52783bcdb1d367ba79c6f1df8bd7e1d03b124))

## 1.0.1 (2025-03-07)

### ♻️ Code Refactoring | 重构

* **custom:** release ([3ddb5be](https://gitee.com/WuCheng-cn/any-thing/commit/3ddb5be0a071f1bd28c265bd4f06679fbd432e02))
* **custom:** release重构 ([0b18811](https://gitee.com/WuCheng-cn/any-thing/commit/0b188113f8b4ba0c8e02afc73f00e65468fe9f44))

### ⚡️ Performance Improvements | 性能优化

* **custom:** 测试 ([586bfb6](https://gitee.com/WuCheng-cn/any-thing/commit/586bfb641e603a1d581f0d475aba5c04f5de055c))
* **custom:** stop tracking .release-it.js ([c40abb6](https://gitee.com/WuCheng-cn/any-thing/commit/c40abb6cd491c340521920c65367c4f0850ec487))
* **decorator:** lazyResize指令功能优化 ([cfb265b](https://gitee.com/WuCheng-cn/any-thing/commit/cfb265bd3b66fe6215023065a036306951c41f96))
* **desktop:** 更生动的弹窗 ([d87b529](https://gitee.com/WuCheng-cn/any-thing/commit/d87b529e2978c6bf36e01f2dad1c8cd72c60133d))
* **desktop:** 任务预览窗口与弹窗内容同步 ([3382a0d](https://gitee.com/WuCheng-cn/any-thing/commit/3382a0dfcf0b4f0364ce51c15321d85bd4e50ea8))
* **desktop:** 首屏优化 ([b6c2290](https://gitee.com/WuCheng-cn/any-thing/commit/b6c22900400f63193f3a5c58c52c4811ea96ce09))
* **desktop:** 图标返回动画优化 ([70d798e](https://gitee.com/WuCheng-cn/any-thing/commit/70d798e40ce7de252fa96027a6dc3ed687b4aea8))
* **desktop:** 新增延迟渲染指令，优化渲染性能；调整弹窗置顶逻辑与触发时机 ([9328732](https://gitee.com/WuCheng-cn/any-thing/commit/93287329d363d805b48756315ee9ad294bcebb6f))
* **desktop:** 延迟渲染指令变更为仅过渡可见性 ([adb6f38](https://gitee.com/WuCheng-cn/any-thing/commit/adb6f386e68633496c5fff275ef43af2e68bc28d))
* **desktop:** 优化打包后Cesium资源载入时机 ([f661d6f](https://gitee.com/WuCheng-cn/any-thing/commit/f661d6f6a1cd920ba6da02b1916775a3274aac4d))
* **desktop:** 桌面应用设计重构 ([e68b6a6](https://gitee.com/WuCheng-cn/any-thing/commit/e68b6a69763e71fac18010b6418ee3c3f9ab03b0))
* **view:** 获取地图实例 ([c4ac36f](https://gitee.com/WuCheng-cn/any-thing/commit/c4ac36fc67c00bab4eebd305edebf92c4eec3cab))

### ✨ Features | 新功能

* **custom:** 测试图标类型 ([6713c4b](https://gitee.com/WuCheng-cn/any-thing/commit/6713c4b59227a6762b9bc28ee257e97510c57b6f))
* **desktop:** 弹窗层级交互 ([7411e20](https://gitee.com/WuCheng-cn/any-thing/commit/7411e208502f18421087f0d95a8c5925ca2f4b9c))
* **desktop:** 电量与pc同步 ([f2f65f4](https://gitee.com/WuCheng-cn/any-thing/commit/f2f65f452a9733e4b95161f4c65165565b26e5c3))
* **desktop:** 任务栏预览窗与联动 ([8d48786](https://gitee.com/WuCheng-cn/any-thing/commit/8d48786df5b6a571766633f693f3767459d67558))
* **desktop:** 新增网络状态显示 ([35cbf59](https://gitee.com/WuCheng-cn/any-thing/commit/35cbf59b4160c698c0e19172094273c710c40e01))
* **desktop:** 一个简单的载入loading ([14e8d9b](https://gitee.com/WuCheng-cn/any-thing/commit/14e8d9bdaadee20f9b9338d2b98ca1c6eacb92ec))
* **desktop:** 支持电量显示 ([2881825](https://gitee.com/WuCheng-cn/any-thing/commit/28818253ddc8dd232afc3b5238530036319098e6))
* **desktop:** 支持任务栏默认应用使用 ([e106abb](https://gitee.com/WuCheng-cn/any-thing/commit/e106abba1c3ceeb4da13c493a16db4333c12c136))
* **desktop:** 支持随机壁纸 ([544faf1](https://gitee.com/WuCheng-cn/any-thing/commit/544faf1aec3bec53ec9f9cd5a6a3a86d5b3e0aa9))
* **desktop:** 桌面 ([40b8bf5](https://gitee.com/WuCheng-cn/any-thing/commit/40b8bf57a28b046eb587bb0d037126cbbe7096ca))
* **desktop:** 桌面应用支持自定义事件与数据持久化 ([8d843c9](https://gitee.com/WuCheng-cn/any-thing/commit/8d843c9d292576c414a45368a7041b99d5b6206b))
* **desktop:** mac弹窗 ([13961c7](https://gitee.com/WuCheng-cn/any-thing/commit/13961c711e6ad0ae3cce8a5a6db14a5909e56f66))
* **graph:** linechart组件新增一个样式配置表单（待完善） ([b2dabf1](https://gitee.com/WuCheng-cn/any-thing/commit/b2dabf1dec34cd4e781003025bfaf7c8554f4269))
* **helper:** 新增大文件切片 ([09b7ab3](https://gitee.com/WuCheng-cn/any-thing/commit/09b7ab3957f52e9bef8add2dc1c8f56ac97fb596))
* **helper:** anyDataHelper新增深度合并方法，新建AnyEchartsHelper（待完善） ([a15e88b](https://gitee.com/WuCheng-cn/any-thing/commit/a15e88bb1265b10560177ffd663a0c75dfb849f7))
* **view:** 飞行航线图 ([31db4b3](https://gitee.com/WuCheng-cn/any-thing/commit/31db4b359402dcb0761d7f1b3a9314c8bc6a3624))
* **view:** 新增登录页 ([ec4b250](https://gitee.com/WuCheng-cn/any-thing/commit/ec4b250f7b35c474fe8f27b83b5ed0dcb283a490))
* **view:** threejs引入 ([38a0eab](https://gitee.com/WuCheng-cn/any-thing/commit/38a0eaba07906822a27d87b1609d78c2935cfa6d))

### 🎨 Styles | 代码样式美化

* **desktop:** hooks抽离，应用接入 ([0f70e18](https://gitee.com/WuCheng-cn/any-thing/commit/0f70e1878f1f5707c6eb778353ffd16ec462f4c2))

### 🐛 Bug Fixes | 修复 bug

* **custom:** cz配置修复 ([799464d](https://gitee.com/WuCheng-cn/any-thing/commit/799464d00cd4827084f451aa8811175c9a35e886))
* **custom:** cz配置修复 ([00aeb7e](https://gitee.com/WuCheng-cn/any-thing/commit/00aeb7e0bec7701adccc2389d2297a811fbc534b))
* **custom:** ts版本回退至5.1.6 ([994833e](https://gitee.com/WuCheng-cn/any-thing/commit/994833e6d2d6ce8878eb4f3d5b4ec9e0911c2dbc))
* **custom:** x ([8f537c9](https://gitee.com/WuCheng-cn/any-thing/commit/8f537c936e61177efc39ce7f3de306b14a2482d0))
* **desktop:** 修复弹窗内容延迟渲染导致的溢出滚动条抖动 ([fbe8511](https://gitee.com/WuCheng-cn/any-thing/commit/fbe8511ba0de65b6a098d41fdb9037b14e6d8359))
* **desktop:** 修复头像资源加载问题 ([0a0def5](https://gitee.com/WuCheng-cn/any-thing/commit/0a0def53f92fb831f6965dbd399ea7853c112666))
* **desktop:** 修复图形引擎加载失败的问题 ([5f72962](https://gitee.com/WuCheng-cn/any-thing/commit/5f72962e4fb75925e228491574fdee96e7df8b9c))
* **graph:** 修复组件缩略图全局样式污染 ([b032a52](https://gitee.com/WuCheng-cn/any-thing/commit/b032a52d80a0c3b84af13e43f54865011384cd75))

### 📝 Documentation | 文档变更

* **custom:** 更新REDEME ([e5a5815](https://gitee.com/WuCheng-cn/any-thing/commit/e5a58158652637a969dce011cebff40ca0fea8d1))

### 📦️ Builds | 打包

* **custom:** 打包发布 ([dcd7743](https://gitee.com/WuCheng-cn/any-thing/commit/dcd774307191b8498b6e0919d6a807a74ae8925e))
* **custom:** 打包发布 ([6457d19](https://gitee.com/WuCheng-cn/any-thing/commit/6457d1973993be90ae4fc00a1099dbecb08097fc))
* **custom:** 打包发布 ([0dd0143](https://gitee.com/WuCheng-cn/any-thing/commit/0dd01437a829696a86c65d55db704d0aa3597775))
* **custom:** 打包文件夹更名为docs，适配github静态部署 ([c114c7a](https://gitee.com/WuCheng-cn/any-thing/commit/c114c7ae1e0905ada292b36706fcb7f845e98f32))
* **custom:** 构建静态包，部署至gitee静态页 ([98b9766](https://gitee.com/WuCheng-cn/any-thing/commit/98b9766c03d2ac8a7b97d2a15499daa942e06d7b))

### 🚀 Chores | 构建/工程依赖/工具

* **custom:** 1.0.0 ([d8cff8d](https://gitee.com/WuCheng-cn/any-thing/commit/d8cff8d5644a75f3e7ddf71cd51c9f0b62e79306))
* **custom:** 测试 ([b62b91c](https://gitee.com/WuCheng-cn/any-thing/commit/b62b91cd2f0eb9980474fc8af8aa8930a247cf9e))
* **custom:** 测试配置 ([a1b1e98](https://gitee.com/WuCheng-cn/any-thing/commit/a1b1e9865bbd87d2624140d06dcb79265b64dbc9))
* **custom:** 跟踪release-it配置文件 ([b161423](https://gitee.com/WuCheng-cn/any-thing/commit/b16142323bf65f50b64c90161cf069f74dfea0f2))
* **custom:** 工程化 ([9fb1d80](https://gitee.com/WuCheng-cn/any-thing/commit/9fb1d80d3258c5ca3e214ed6f958d40b854fc644))
* **custom:** 配置变更 ([87dba75](https://gitee.com/WuCheng-cn/any-thing/commit/87dba75e5e66606780eec9166647cad1aff89a03))
* **custom:** 配置变更 ([6fa7288](https://gitee.com/WuCheng-cn/any-thing/commit/6fa728806f26d3b46813ee1ccb222df99f5453b8))
* **custom:** 配置变更 ([5cb42e7](https://gitee.com/WuCheng-cn/any-thing/commit/5cb42e7af2a82cc0c945b57ba75e197320760ac7))
* **custom:** 配置变更 ([2e3f953](https://gitee.com/WuCheng-cn/any-thing/commit/2e3f953543eb181313fd588a9b8a4e89054d214e))
* **custom:** 配置变更 ([dbf6f36](https://gitee.com/WuCheng-cn/any-thing/commit/dbf6f36057db4775ee4645323b6f0afe30ae774b))
* **custom:** 自定义release配置 ([33487e2](https://gitee.com/WuCheng-cn/any-thing/commit/33487e202161a5e238d6380ae7916d049c1d0cf0))
* **custom:** release 配置测试 ([fcbcfb9](https://gitee.com/WuCheng-cn/any-thing/commit/fcbcfb9300e73c9b5c45b6f2ef9477fec75bab6c))
* **custom:** release工具变更 ([8f6b1fd](https://gitee.com/WuCheng-cn/any-thing/commit/8f6b1fdeaec92d98d1ec1109ce156f3243380148))
* **custom:** test ([65737f0](https://gitee.com/WuCheng-cn/any-thing/commit/65737f0e17d6359b7b15d0440a5a47f9815d28b0))
* **custom:** xx ([6497644](https://gitee.com/WuCheng-cn/any-thing/commit/64976447fe1930e807da8a55e3f850343488d18d))
* **desktop:** 静态构建，部署至gitee ([b19326b](https://gitee.com/WuCheng-cn/any-thing/commit/b19326b7877c2b84b225858e9ef36ec98093396e))
* **release:** Release v1.5.1 ([3a39de6](https://gitee.com/WuCheng-cn/any-thing/commit/3a39de69b9771a6c99e4c7a2b6e5e437a6883b49))
* **release:** Release v1.5.10 ([36cba57](https://gitee.com/WuCheng-cn/any-thing/commit/36cba575536bf921d9166ea24d6faa7ef075dbd2))
* **release:** Release v1.5.13 ([9de8c3d](https://gitee.com/WuCheng-cn/any-thing/commit/9de8c3d23d3a2314eec6012b3e8b791ba33dbd0d))
* **release:** Release v1.5.2 ([391db92](https://gitee.com/WuCheng-cn/any-thing/commit/391db929f2ff276b23384d8393f44e588722a477))
* **release:** Release v1.5.3 ([53905ea](https://gitee.com/WuCheng-cn/any-thing/commit/53905eac3ac184e11102298201379c5a70fa2264))
* **release:** Release v1.5.7 ([6381b03](https://gitee.com/WuCheng-cn/any-thing/commit/6381b03563e5a198bc896eb9d0a5dbc44076820a))
